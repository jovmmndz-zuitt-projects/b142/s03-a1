package com.mendez.b142.s02.s02app.services;

import com.mendez.b142.s02.s02app.models.User;

public interface UserService {
    void createUser(User newUser);
    void updateUser(Long id, User updatedUser);
    void deleteUser(Long id);
    Iterable<User> getUsers();
}


/*
package com.mendez.b142.s02.s02app.services;

        import com.mendez.b142.s02.s02app.models.Post;

public interface PostService {
    void createPost(Post newPost);
    void updatePost(Long id, Post updatedPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
}

 */